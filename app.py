from flask import Flask
from flask_cors import CORS,cross_origin
from end_points.distance import distance_blueprint
app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
# @cross_origin()
app.register_blueprint(distance_blueprint, url_prefix='/distance')


