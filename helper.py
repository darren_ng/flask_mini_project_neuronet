import re
from math import pi, sin, cos, atan2, sqrt

from shapely.geometry import Point


def deg_2_rad(deg: float) -> float:
    """
    Convert degree to radian
    :param deg:
    :return:
    """
    if type(deg) not in (float, int):
        raise TypeError("Input is supposed to be float/int format!")
    if abs(deg) > 180:
        raise ValueError("Degree must be within range [-180,180]")
    return deg * (pi / 180)


def calculate_distance(lat1: float, long1: float, lat2: float, long2: float):
    """
    Calculate distance from 2 geo coordinates
    :param lat1: location 1 latitude
    :param long1: location 1 longitude
    :param lat2: location 2 latitude
    :param long2: location 2 longitude
    :return: float: distance in kms
    """
    if not all([isinstance(x, (int, float)) for x in [lat1, lat2, long1, long2]]):
        raise TypeError("Some argument is not float!")
    if any([abs(lat) > 90 for lat in {lat1, lat2}]):
        raise ValueError("Maximum value for latitude is 90!")
    if any([abs(long) > 180 for long in [long1, long2]]):
        raise ValueError("Maximum value for longtitude is 180!")
    R = 6731  # Earth diameter in km
    d_lat = deg_2_rad(lat2 - lat1)
    d_long = deg_2_rad(long2 - long1)
    a = sin(d_lat / 2) * sin(d_lat / 2) \
        + cos(deg_2_rad(lat1)) * cos(deg_2_rad(lat2)) * sin(d_long / 2) * sin(d_long / 2)
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    d = R * c  # Distance in km
    return d


def parse_loc(loc: str) -> Point:
    """
    convert str to Point
    :param loc: latitude and longtitude separate by a space
    :return: Point
    """
    pattern = "^[0-9.]+\s[0-9.]+$"
    if not isinstance(loc, str):
        raise TypeError(" Input is supposed to be string type!")
    matched = re.match(pattern, loc)
    if not matched:
        raise ValueError("Wrong format!")
    loc1, loc2 = loc.split(' ')
    return Point(float(loc1), float(loc2))
