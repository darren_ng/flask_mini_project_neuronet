import configparser
import logging
import os
import time

import requests
from flask import Blueprint, request, jsonify

from MKAD import MKAD
from helper import parse_loc

distance_blueprint = Blueprint('/', __name__)
config = configparser.ConfigParser()
config.read('./config.ini')

log_file = os.path.join(config['log']['LOG_DIR'], "distance.log")
logging.basicConfig(
    filename=log_file,
    filemode="w",
    format="%(asctime)s [Line %(lineno)d] [%(levelname)s] %(message)s",
    level=logging.INFO
)
logger = logging.getLogger()
params = {
    'apikey': config['yandex']['API_KEY'],
    'format': config['yandex']['FORMAT'],
    'lang': config['yandex']['LANG']
}
geo_api = config['yandex']['URL']
MKAD = MKAD(config)


@distance_blueprint.route("/", methods=["GET", "POST"])
def index():
    if request.method == "GET" or request.method == "POST":
        if request.method == "GET":
            params['geocode'] = request.args.get("location")
        elif request.method == "POST":
            params['geocode'] = request.json['location']
        logger.info("Receive request for {}".format(params["geocode"]))
        success = False
        retries = 1
        while not success and retries < 5:
            try:
                r = requests.get(geo_api, params)
                success = True
            except Exception as e:
                wait = retries * 5
                logger.warning("{} Request timed out! Retry after {} sec!".format(geo_api, wait))
                time.sleep(wait)
                retries += 1
        if not success:
            logger.error("Cannot retrieve geo information for {}".format(params['geocode']))
            return jsonify({"code": 404,
                            "message": "Cannot retrieve geo information for {}".format(params['geocode'])})
        # print(r.json())
        location = r.json()["response"]['GeoObjectCollection']['featureMember'][0]["GeoObject"]["Point"]["pos"]
        coordinate = parse_loc(location)
        if MKAD.contains(coordinate):
            return jsonify({"code": 204,
                            "message": "The location is inside MKAD"})
        return jsonify({"code": 200,
                        "distance": MKAD.distance(coordinate),
                        "unit": "km"})
    else:
        return jsonify({"code": 405,
                        "message": "Request type not supported"})
