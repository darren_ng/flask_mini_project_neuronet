from shapely.geometry import Polygon
import requests
from helper import calculate_distance, parse_loc, Point
from configparser import ConfigParser


class MKAD:
    def __init__(self, config: ConfigParser):
        url = config['yandex']['URL']
        params = {'apikey': config['yandex']['API_KEY'],
                  'format': config['yandex']['FORMAT'],
                  'lang': config['yandex']['LANG'],
                  'geocode': 'MKAD'
                  }

        r = requests.get(url, params)
        json_obj = r.json()
        result_members = json_obj['response']['GeoObjectCollection']['featureMember']
        MKAD_points = []
        for mem in result_members:
            if 'RU' in mem["GeoObject"]["metaDataProperty"]["GeocoderMetaData"]["Address"]["country_code"]:
                MKAD_points.append(mem["GeoObject"]["Point"]["pos"])
        self.location = [parse_loc(a) for a in MKAD_points]
        self.area = Polygon(self.location)

    def contains(self, coordinate: Point) -> bool:
        return self.area.contains(coordinate)

    def distance(self, coordinate: Point) -> float:
        return min([calculate_distance(c.x, c.y, coordinate.x, coordinate.y) for c in self.location])