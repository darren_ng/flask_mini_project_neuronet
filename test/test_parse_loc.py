import unittest

from shapely.geometry import Point

from helper import parse_loc


class TestParseLoc(unittest.TestCase):
    def test_input_value(self):
        self.assertRaises(TypeError, parse_loc, True)
        self.assertRaises(TypeError, parse_loc, 1)
        self.assertRaises(TypeError, parse_loc, 23.6)
        self.assertRaises(TypeError, parse_loc, [1])

    def test_parse_loc(self):
        self.assertEqual(parse_loc("40 50"), Point(40, 50))
        self.assertEqual(parse_loc("49.5 50.5"), Point(49.5, 50.5))
        self.assertEqual(parse_loc("-11 23"), Point(-11, 23))
        self.assertEqual(parse_loc("66.66 77.3"), Point(66.66, 77.3))
        self.assertRaises(ValueError, parse_loc, "40 abc")
        self.assertRaises(ValueError, parse_loc, "4a0 555")
        self.assertRaises(ValueError, parse_loc, '')


if __name__ == '__main__':
    unittest.main(verbosity=2)
