import unittest

from helper import calculate_distance


class TestCalculateDistance(unittest.TestCase):
    def test_input_value(self):
        self.assertRaises(TypeError, calculate_distance, True, False, 2, "a")
        self.assertRaises(ValueError, calculate_distance, 300, 20, 99, 55)
        self.assertRaises(ValueError, calculate_distance, 2, 2, 500, 698)

    def test_distance(self):
        self.assertEqual(
            calculate_distance(81.76346651200049, 117.66664547410937, 41.34216342753163, -37.28632969108597),
            6602.243935701124)
        self.assertEqual(
            calculate_distance(37.30865139050664, 31.33826361643068, 53.98058573424254, -119.46230447278067),
            10020.902206008825)
        self.assertEqual(
            calculate_distance(10.829451264773212, -88.6335106848325, 76.99699035462288, -154.86177902025125),
            8717.785280392103)
        self.assertEqual(
            calculate_distance(66.0211343584505, 20.513786498051246, 6.677463366841778, -107.42311695865159),
            11531.477558346955)
        self.assertEqual(
            calculate_distance(14.970311272487734, -25.7147986140086, 38.3576313134339, 107.36794105990873),
            13031.18445726839)
        self.assertEqual(
            calculate_distance(-81.47628433281982, 23.950492370375656, -87.96002156230453, -125.76854334687042),
            1214.2885856476134)


if __name__ == '__main__':
    unittest.main(verbosity=2)
