import unittest

from helper import deg_2_rad


class TestDeg2Rad(unittest.TestCase):
    def test_input_value(self):
        self.assertRaises(TypeError, deg_2_rad, True)
        self.assertRaises(TypeError, deg_2_rad, '1')
        self.assertRaises(TypeError, deg_2_rad, '')
        self.assertRaises(TypeError, deg_2_rad, [2])
        self.assertRaises(ValueError, deg_2_rad, -181)
        self.assertRaises(ValueError, deg_2_rad, 200)

    def test_rad(self):
        self.assertEqual(deg_2_rad(131.54625264020092), 2.2959152272317898)
        self.assertEqual(deg_2_rad(6.431558478733308), 0.11225187148845392)
        self.assertEqual(deg_2_rad(-14.921505581008034), -0.26042940174329965)
        self.assertEqual(deg_2_rad(-167.3879419534137), -2.921470715224218)
        self.assertEqual(deg_2_rad(155.07966697316652), 2.7066507915780647)
        self.assertEqual(deg_2_rad(-86.65593860353), -1.5124314450376557)


if __name__ == '__main__':
    unittest.main(verbosity=2)
