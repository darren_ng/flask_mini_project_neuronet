# flask_mini_project_neuronet

Personal flask project

#Directory structure
```
flask_mini_project_neuronet/
├── app.py
├── config.ini
├── end_points
│   └── distance.py
├── helper.py
├── log
├── MKAD.py
├── README.md
├── test
│   ├── __init__.py
│   ├── test_calc_distance.py
│   ├── test_deg2rad.py
│   └── test_parse_loc.py
```
##Description:
-`log`: store log file
-`end_points`: store blueprint
-`test`: unittest

-`app.py`: main flask app
-`helper.py`: helper functions
-`MKAD.py`: MKAD class
-`config.ini`: configuration file

## Package used
-`shapely`
-`flask`
-`requests`

## How to run
Install dependency:
- `pip install -r requirements.txt`

Activate your virtualenv:
- `source your_env/bin/activate`

## Running testcases
- `python -m test.test_case`
